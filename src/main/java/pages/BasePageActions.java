package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import tests.TestBaseConfiguration;

public class BasePageActions extends TestBaseConfiguration{


	//Method to click on an element
	protected static void clickOnElement(By element)
	{
		locateElement(element).click();
	}

	//Method to wait for an element to be visible
	public static void waitForElement(By element)
	{
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.visibilityOf(locateElement(element)));
	}

	//Method to type in an element
	public static void typeOnElement(By element , String KeysToSend)
	{
		locateElement(element).sendKeys(KeysToSend);
	}

	//Method to locate an element
	public static WebElement locateElement(By element)
	{
		return driver.findElement(element);
	}

	//Method to select an element by index
	public static void selectElement(By element , int index)
	{
		WebElement list = locateElement(element);
		Select selectOption = new Select(list);
		selectOption.selectByIndex(index);
	}

	public static void moveToElement(By element,String phone)
	{
		WebElement locatedElement= locateElement(element);
		locatedElement.sendKeys(Keys.ENTER);
		Actions builder = new Actions(driver);
		//builder.click(locatedElement).perform();
		builder.sendKeys(locatedElement,phone).perform();  
		

	}
}
