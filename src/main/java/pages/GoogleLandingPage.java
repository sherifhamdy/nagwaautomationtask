package pages;

import org.openqa.selenium.By;


public class GoogleLandingPage extends BasePageActions
{

	public static By signInBTN = By.id("gb_70");

	public static void ClickOnSignInButton()
	{
		clickOnElement(signInBTN);
	}

}
