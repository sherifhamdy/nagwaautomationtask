package pages;

import org.openqa.selenium.By;

public class SignUpPage extends BasePageActions
{

	// Object Repo POM
	public static By signUpBTN = By.xpath(".//*[@class='OIPlvf']//span");

	public static By firstName = By.id("firstName");
	static By lastName = By.id("lastName");
	static By email = By.id("username");
	static By pass = By.name("Passwd");
	static By confPass = By.name("ConfirmPasswd");
	static By nextBTN = By.xpath(".//*[@id='accountDetailsNext']//span");
	static By secondNextBTN = By.xpath(".//*[@class='CwaK9']//span");
	static By phoneNumField = By.id("phoneNumberId");
	static By alternativeMail = By.xpath("//*[@id='view_container']/form/div[2]/div/div[2]/div/div[1]/div/div[1]/input");
	static By Day = By.id("day");
	static By Month = By.id("month");
	static By Year = By.id("year");
	static By Gender = By.id("gender");

	public static void ClickOnCreateNewAccount()
	{
		waitForElement(signUpBTN);
		clickOnElement(signUpBTN);
	}



	//Method to fill the data required for account creation for two Forms 
	public static void FillSignUpForm(String FN,String LN,String Email,String Pass,String confpass)
	{
		FillSignUpForm1( FN, LN, Email,Pass, confpass);
		clickOnElement(nextBTN);	

	}


	//Method for filling FORM 1 SIGN UP
	private static void FillSignUpForm1(String FN,String LN,String Email,String Pass,String confpass)
	{

		waitForElement(firstName);
		typeOnElement(firstName, FN);
		typeOnElement(lastName, LN);
		typeOnElement(email, Email);
		typeOnElement(pass, Pass);
		typeOnElement(confPass, confpass);


	}
	//Method for filling FORM 2 SIGN UP
	public static void FillSignUpForm2(String phone,String Alemail,String day,int month,String year,int gender)
	{

		typeOnElement(phoneNumField, phone);
		typeOnElement(alternativeMail, Alemail);
		typeOnElement(Day, day);
		selectElement(Month, month-1);
		typeOnElement(Year, year);
		selectElement(Gender,gender);			


	}

	//Method for filling FORM3 SIGN UP
	public static void FillSignUpForm3(String phone) 
	{
		waitForElement(phoneNumField);
		//driver.switchTo().defaultContent();
		//typeOnElement(phoneNumField, phone);
		//moveToElement(phoneNumField,phone);
		//clickOnElement(secondNextBTN);
	}
	//Method to submit the data for account creation
	public static void CreateTheAccount()
	{
		clickOnElement(nextBTN);
	}

}
