package utilities;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Helper {

	//Method to take screenshot when Test cases Fail
	public static void captureFAIL_Screenshot(WebDriver driver , String screenshotname)
	{
		//Save the capture screen shot into the destination created folder with the name of the test case 
		Path dest = Paths.get("./Fail_Screenshots",screenshotname+".png");
		try {
			Files.createDirectories(dest.getParent());
			FileOutputStream out = new FileOutputStream(dest.toString());
			out.write(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES));
			out.close();
		} catch (IOException e) {
			System.out.print("Exception while taking screen shot" + e.getMessage());
		}
	}


	//Method to take screenshot when Test cases Pass
	public static void capturePASS_Screenshot(WebDriver driver , String screenshotname)
	{
		//Save the capture screen shot into the destination created folder with the name of the test case 
		Path dest = Paths.get("./Pass_Screenshots",screenshotname+".png");
		try {
			Files.createDirectories(dest.getParent());
			FileOutputStream out = new FileOutputStream(dest.toString());
			out.write(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES));
			out.close();
		} catch (IOException e) {
			System.out.print("Exception while taking screen shot" + e.getMessage());
		}
	}

}
