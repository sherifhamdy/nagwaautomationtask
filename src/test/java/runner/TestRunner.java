package runner;

import cucumber.api.CucumberOptions;
import tests.TestBaseConfiguration;

@CucumberOptions(features="src/test/java/features"
,glue= {"steps"}
,plugin= {"pretty","html:target/cucumber-html-report"}
,tags= "@tag1")
public class TestRunner  extends TestBaseConfiguration{

}
