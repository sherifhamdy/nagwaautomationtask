package tests;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import data.ExcelReader;
import pages.GoogleLandingPage;
import pages.SignInPage;
import pages.SignUpPage;

public class TestCases  extends TestBaseConfiguration
{

	
	//Pre-Condition : Google WebSite is launched
	//,dataProvider="ExcelData"
	@Test(priority=1,enabled=true)
	public void GoogleRegistration(String FN,String LN,String Email, String Pass, String confpass) throws InterruptedException
	{

		GoogleLandingPage.ClickOnSignInButton();
		SignUpPage.ClickOnCreateNewAccount();
		SignUpPage.FillSignUpForm(FN, LN, Email, Pass, confpass);

		//SignUpPage.FillSignUpForm(FN, LN, Email, Pass, confpass, phone, Alemail, day, month, year, gender);
		//SignUpPage.CreateTheAccount();
	}

	@Test(priority=2,enabled=false,dataProvider="ExcelData",dependsOnMethods={"GoogleRegistration"})
	public void GoogleLogin(String Email,String Password) throws InterruptedException
	{
		//Not implemented yet !
		SignInPage.loginWith(Email,Password);
		boolean actual = SignInPage.loggedInSuccessfully();
		boolean expected = true;
		Assert.assertEquals(actual, expected);
	}


	/*
	//Method to get the data from a specific source 
	@DataProvider(name="ExcelData")
	public Object[][] userRegisterData() throws IOException
	{
			// get data from Excel Reader class 
		ExcelReader ER = new ExcelReader();
		return ER.getExcelData();


		//Object for a static data
		return new Object[][]
				{

			//Full data
			//{"FirstNameTest","LastNameTest","s9972475@gmail.com","pa$$worD123","pa$$worD123","01117777777","ALEMail_Test@gmail.com","17",5,"1993",1}

			{"FirstNameTest","LastNameTest","s9972475@gmail.com","pa$$worD123","pa$$worD123"}		

				};
	}

	 */
}
