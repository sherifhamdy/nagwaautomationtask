package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import cucumber.api.testng.AbstractTestNGCucumberTests;
import utilities.Helper;

public class TestBaseConfiguration extends AbstractTestNGCucumberTests{

	protected static WebDriver driver;
	protected static String APP_URL = "https://www.google.com.eg/";

	// initialize google Chrome_driver
	@BeforeSuite
	protected static WebDriver setup()
	{
		System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to(APP_URL);
		return driver;
	}

	// close the browser
	@AfterSuite
	protected static void quitBrowser()
	{
		driver.quit();
	}

	//Take Screen-shot when test case fails and add it to the screen-shot folder
	@AfterMethod
	public static void screenshotOnFailure(ITestResult result)
	{
		if(result.getStatus() == ITestResult.FAILURE)
		{
			System.out.println("Failed");
			System.out.println("taking Fail Screenshot...");
			Helper.captureFAIL_Screenshot(driver, result.getName());
		}

	}
}
