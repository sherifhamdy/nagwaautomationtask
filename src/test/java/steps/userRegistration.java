package steps;

import org.testng.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.BasePageActions;
import pages.GoogleLandingPage;
import pages.SignUpPage;

public class userRegistration
{
	@Given("^The user in the google homepage$")
	public void the_user_in_the_google_homepage() 
	{
		boolean actual = BasePageActions.locateElement(GoogleLandingPage.signInBTN).isDisplayed();
		Assert.assertEquals(actual, true);
	}

	@When("^User click on login button$")
	public void user_click_on_login_button()
	{
		GoogleLandingPage.ClickOnSignInButton();
		boolean actual = BasePageActions.locateElement(SignUpPage.signUpBTN).isDisplayed();
		Assert.assertEquals(actual, true);
	}

	@When("^User click on sign up button$")
	public void user_click_on_sign_up_button() 
	{
		SignUpPage.ClickOnCreateNewAccount();
		boolean actual = BasePageActions.locateElement(SignUpPage.firstName).isDisplayed();
		Assert.assertEquals(actual, true);	
	}

	@When("^User enter valid registration data \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\"$")
	public void user_enter_valid_registration_data(String FirstName, String LastName, String Email, String Password, String ConfirmPassword) throws Throwable {
		SignUpPage.FillSignUpForm(FirstName,LastName, Email, Password, ConfirmPassword);
	}

	/*@When("^User enter valid registration data <FirstName>,<LastName>,<Email>,<Password>,<ConfirmPassword>$")
	public void user_enter_valid_registration_data_FirstName_LastName_Email_Password_ConfirmPassword(String FirstName, String LastName, String Email, String Password, String ConfirmPassword) throws Throwable {
		SignUpPage.FillSignUpForm(FirstName,LastName, Email, Password, ConfirmPassword);
	}*/

	@Then("^User successfully registered$")
	public void user_successfully_registered() throws Throwable {

	}

	@Given("^I want to write a step with name$")
	public void i_want_to_write_a_step_with_name() 
	{

	}

	@When("^I check for the value in step$")
	public void i_check_for_the_value_in_step()
	{

	}

	@Then("^I verify the status in step$")
	public void i_verify_the_status_in_step() 
	{

	}

}
