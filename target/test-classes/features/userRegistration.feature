
@tag
Feature: Google User Registration
  I want to check that the user can register and create a new account

  @tag1
  Scenario: Google User Registration
    Given The user in the google homepage
    When User click on login button
    And User click on sign up button
    And User enter valid registration data "FirstNameTest","LastNameTest","s9972475@gmail.com","pa$$worD123","ConfirmPassword"  
    Then User successfully registered

  @tag2
  Scenario Outline: Title of your scenario outline
    Given I want to write a step with name
    When I check for the value in step
    Then I verify the status in step

    Examples: 
      | FirstName | LastName | Email  						| Password 		| ConfirmPassword |
      | Sherif 		| Hussein	 | s9972475@gmail.com | pa$$worD123 | pa$$worD123 		|
