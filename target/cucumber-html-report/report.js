$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("userRegistration.feature");
formatter.feature({
  "line": 3,
  "name": "Google User Registration",
  "description": "I want to check that the user can register and create a new account",
  "id": "google-user-registration",
  "keyword": "Feature",
  "tags": [
    {
      "line": 2,
      "name": "@tag"
    }
  ]
});
formatter.scenario({
  "line": 7,
  "name": "Google User Registration",
  "description": "",
  "id": "google-user-registration;google-user-registration",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 6,
      "name": "@tag1"
    }
  ]
});
formatter.step({
  "line": 8,
  "name": "The user in the google homepage",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "User click on login button",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "User click on sign up button",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "User enter valid registration data \"FirstNameTest\",\"LastNameTest\",\"s9972475@gmail.com\",\"pa$$worD123\",\"ConfirmPassword\"",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "User successfully registered",
  "keyword": "Then "
});
formatter.match({
  "location": "userRegistration.the_user_in_the_google_homepage()"
});
formatter.result({
  "duration": 156844179,
  "status": "passed"
});
formatter.match({
  "location": "userRegistration.user_click_on_login_button()"
});
formatter.result({
  "duration": 2018730241,
  "status": "passed"
});
formatter.match({
  "location": "userRegistration.user_click_on_sign_up_button()"
});
formatter.result({
  "duration": 1635575273,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "FirstNameTest",
      "offset": 36
    },
    {
      "val": "LastNameTest",
      "offset": 52
    },
    {
      "val": "s9972475@gmail.com",
      "offset": 67
    },
    {
      "val": "pa$$worD123",
      "offset": 88
    },
    {
      "val": "ConfirmPassword",
      "offset": 102
    }
  ],
  "location": "userRegistration.user_enter_valid_registration_data(String,String,String,String,String)"
});
formatter.result({
  "duration": 3150175877,
  "status": "passed"
});
formatter.match({
  "location": "userRegistration.user_successfully_registered()"
});
formatter.result({
  "duration": 30359,
  "status": "passed"
});
});